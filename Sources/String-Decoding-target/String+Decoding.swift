import Foundation

public extension String {
    var base64Decode: String? {
        let fixedSelf = padding(toLength: ((self.count + 3) / 4) * 4, withPad: "=", startingAt: 0)
        guard let data = Data(base64Encoded: fixedSelf) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
