import XCTest
@testable import String_Decoding

final class String_DecodingTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(String_Decoding().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
