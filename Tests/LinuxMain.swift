import XCTest

import String_DecodingTests

var tests = [XCTestCaseEntry]()
tests += String_DecodingTests.allTests()
XCTMain(tests)
